<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/resources/css/nav.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>My Pets</title>
</head>
<body>
	<nav class="navbar navbar-dark">
	 <div class="container-fluid">
		<ul class="nav navbar-nav">
			<li class="active"><a href="./allpets">Home</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><p  class="navbar-text">Welcome, <%=(String)request.getSession().getAttribute("username") %></p></li>
			<li><a href="./userpets">My Pets</a></li>
			<li><a href="./addPet">Add Pet</a></li>
			<li><a href="./logout"><span
					class="glyphicon glyphicon-log-in"></span>Logout</a></li>
		</ul>
	  </div>
	</nav>
	<div class="container">
	<table class="table table-striped">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">PetName</th>
				<th scope="col">Age</th>
				<th scope="col">Place</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="pet" items="${requestScope.mypets}">
				<tr>
					<td>${pet.getId()}</td>
					<td>${pet.getName()}</td>
					<td>${pet.getAge()}</td>
					<td>${pet.getPlace()}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	
	
	
</body>
</html>