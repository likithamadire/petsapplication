package com.hcl.pp.customException;

import org.apache.log4j.Logger;

public class UserException extends Exception {

	private String message;
	private static final Logger LOGGER = Logger.getLogger(UserException.class);
	
	public UserException(String message) {
		super();
		this.message = message;
		LOGGER.error("UserException: "+this.message);
	}

}
