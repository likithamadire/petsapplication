package com.hcl.pp.validator;

import com.hcl.pp.model.Pet;

public class PetValidator {

	public String validate(Pet pet) {
		
		if(pet!=null) {
			if(pet.getName()==null || pet.getPlace()==null) {
				return "Either petname or petplace should not empty";
			}else if(pet.getAge()<0 || pet.getAge()>99) {
				return "Pet age should between 0 and 99";
			}
		}
		return "Pet has validated successfully";
	}
}
