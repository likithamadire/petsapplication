package com.hcl.pp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.model.Pet;

@Repository("petDaoImpl")
public class PetDaoImpl implements PetDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	Session session = null;
	public Session getSession() {
		try {
			session = sessionFactory.getCurrentSession();
		}catch(Exception e) {
			session = sessionFactory.openSession();
		}
		return session;
	}
	
	public Pet getPetById(Long id) throws UserException {
		
		Session session1 =getSession();
		List<Pet> list = null;
		Pet pet = new Pet();
		String hql = "from pets p where p.id= :idVal";
		if(!hql.isEmpty()) {
			Query query = session1.createQuery(hql);
			query.setParameter("idVal", id);
			list = query.list();
			for (Pet petlist : list) {
				pet.setId(id);
				pet.setAge(petlist.getAge());
				pet.setName(petlist.getName());
				pet.setPlace(petlist.getPlace());
			}
			session1.close();
			return pet;
		}
		else {
			throw new UserException("No pet find by id");
		}
	}

	public void savePet(Pet pet) {
		
		Session session1 =getSession();
		session1.save(pet);	
	}

	public List<Pet> fetchAll() throws UserException {
		
		List<Pet> pets=null;
		Session session1 =getSession();
		String hql = "from pets";
		if(!hql.isEmpty()) {
			Query query = session1.createQuery(hql);
			pets = query.getResultList();
			return pets;
		}
		else {
			throw new UserException("No pets found");
		}	
	}

}
