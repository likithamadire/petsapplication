package com.hcl.pp.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.dao.UserDao;
import com.hcl.pp.model.User;

@Service
public class SecurityServiceImpl implements SecurityService{

	
	@Autowired
	private UserDao userDao;
	@Override
	public boolean authenticateUser(User userDetails) throws UserException {
		
		List<User> listUsers= userDao.listUsers();
		HttpServletRequest request = null;
		for (User user : listUsers) {
			if(user.getUsername().equals(userDetails.getUsername()) && user.getUserPassword().equals(userDetails.getUserPassword())) {
				//request.getSession().setAttribute("username", userDetails.getUsername());
				return true;			
			}
		}
		return false;
	}

}
